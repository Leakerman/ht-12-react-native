import { createStackNavigator } from '@react-navigation/stack';
import { ContactList } from '../containers/ContactList';
import React from 'react'
import ContactPage from '../containers/ContactPage';

const Stack = createStackNavigator();

export function StackNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Contacts"
      headerMode="screen"
    >
      <Stack.Screen
        name="Contacts"
        component={ContactList}

      />
      <Stack.Screen
        name="Contact"
        component={ContactPage}
      />
    </Stack.Navigator>
  );
}