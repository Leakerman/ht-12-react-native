import React, { useEffect, useState } from "react";
import * as Contacts from "expo-contacts";
import {
  Text,
  View,
  FlatList,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";

export function ContactList(props) {
  const [contacts, setContacts] = useState([]);
  const [contactsToShow, setContactsToShow] = useState([]);
  const [searchContactText, setSearchContactText] = useState("");

  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        let { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.PhoneNumbers],
        });

        data = data.filter((contact) => {
          if (contact.phoneNumbers) {
            return (
              contact.firstName &&
              contact.phoneNumbers.length &&
              contact.phoneNumbers[0].number
            );
          }
        });
        setContacts(data);
        setContactsToShow(data);
      }
    })();
  }, []);

  const onChange = (text) => {
    setSearchContactText(text);
    setContactsToShow(contacts.filter((c) => c.firstName.includes(text)));
  };

  const onContactPress = (id) => {
    props.navigation.navigate("Contact", { contactId: id });
  };

  const createContact = ({ item: { id, firstName: name, phoneNumbers } }) => {
    return (
      <TouchableOpacity onPress={() => onContactPress(id)}>
        <View style={styles.contact}>
          <Text style={{ fontSize: 17, maxWidth: 150 }}>{name}</Text>
          <Text style={{ fontSize: 17 }}>{phoneNumbers[0].number}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        paddingVertical: 10,
        paddingHorizontal: 20,
      }}
    >
      <TextInput
        style={styles.searchContacts}
        placeholder="Search contacts"
        value={searchContactText}
        onChangeText={onChange}
      />
      {contacts.length ? (
        <FlatList
          data={contactsToShow}
          renderItem={createContact}
          keyExtractor={(item) => item.id}
        />
      ) : (
        <Text>There is no contacts</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  searchContacts: {
    borderWidth: 1,
    borderColor: "#20232a",
    borderRadius: 6,
    marginBottom: 10,
    paddingHorizontal: 5,
  },
  contact: {
    borderWidth: 1,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: "#cecece",
    color: "#333",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 5,
    paddingHorizontal: 10,
    alignItems: "center",
    flexDirection: "row",
    fontSize: 12,
  },
});
