import React, { useEffect, useState, Fragment } from "react";
import { StyleSheet, Image, Text, View, Linking, Button } from "react-native";
import * as Contacts from "expo-contacts";

function ContactPage(props) {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    (async () => {
      const currUser = await Contacts.getContactByIdAsync(
        props.route.params.contactId
      );
      setUser(currUser);
      setLoading(false);
    })();
  }, []);

  function onCall(number) {
    (async () => {
      const url = `tel:${number}`;
      await Linking.openURL(url);
    })();
  }

  function onDelete(id) {
    (async () => {
      try {
        // user wouldn't deleted because og luck of write contacts permission
        await Contacts.removeContactAsync(id);
      } catch (error) {
        console.error(error);
      }
    })();
  }

  return (
    <Fragment>
      {loading ? (
        <Text>Loading...</Text>
      ) : (
        <View style={styles.contact}>
          <View style={{ marginTop: 10 }}>
            <Image
              style={styles.image}
              source={(() => {
                const uri = user.imageAvailable
                  ? user.image.uri
                  : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png";
                return { uri };
              })()}
            />
            <View style={styles.infoContainer}>
              <Text>{user.firstName || user.name}</Text>
              <Text>{user.phoneNumbers[0].number}</Text>
            </View>
          </View>
          <View style={{ width: "100%" }}>
            <Button
              color="green"
              title="Call"
              onPress={() => onCall(user.phoneNumbers[0].number)}
            />
            <Button
              color="grey"
              title="Delete"
              onPress={() => onDelete(user.id)}
            />
          </View>
        </View>
      )}
    </Fragment>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 320,
    height: 320,
    backgroundColor: "#333",
    borderRadius: 10,
    borderWidth: 3,
    borderColor: "#333",
  },
  contact: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  infoContainer: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

export default ContactPage;
